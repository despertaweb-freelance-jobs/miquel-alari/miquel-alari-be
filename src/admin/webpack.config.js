"use strict";

const webpack = require("webpack");

/* eslint-disable no-unused-vars */
module.exports = (config, all) => {
  // Perform customizations to webpack config
  // Important: return the modified config

  config.plugins.push(
    new webpack.NormalModuleReplacementPlugin(
      /^tippy\.js$/,
      "tippy.js/dist/tippy-bundle.umd.min.js"
    )
  );

  return config;
};
